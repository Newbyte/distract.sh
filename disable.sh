#!/bin/sh

source ./shared.sh

while IFS= read -r host; do
    sed -i "/0\.0\.0\.0 $host/d" $HOSTFILE_PATH
done < $BLOCKED_HOSTS

