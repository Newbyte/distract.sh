# distract.sh

## Getting started

distract.sh works by reading from one file and writing to another. By default it reads from `blocked-hosts` in the current directory and writes to `/etc/hosts`. The filepaths for these can be changed in `shared.sh`.

To get started, create a file called `blocked-hosts` in the directory you intend to run the scripts from. This file should contain a list of hosts you wish to block, each on their own line (in other words, separated by newlines).

`blocked-hosts` should look something like this: 

```
twitter.com
youtube.com
reddit.com
```

In order to block the hosts you run `enable.sh` from the same directory as `blocked-hosts` is located. Doing this will add your specified hosts to `/etc/hosts`, redirecting to `0.0.0.0`. This effectively blocks the hosts as `0.0.0.0` will respond with nothing. 

To unblock the hosts, run `disable.sh`. 

Note that these scripts should never remove or alter any hosts aside from those specified in `blocked-hosts`. Any instances of this occuring are considered bugs and should be reported.

## Troubleshooting

### I can still access the websites I've blocked in my browser!

This is likely due to that your browser keeps its own DNS cache. Look up how to clear your specific browser's DNS cache to work around this. 

