#!/bin/sh

source ./shared.sh

while IFS= read -r host; do
    if ! grep -F make_hosts_entry $host $HOSTFILE_PATH 2> /dev/null
    then
        make_hosts_entry $host >> $HOSTFILE_PATH
    fi
done < $BLOCKED_HOSTS

